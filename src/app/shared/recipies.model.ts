export class Recipies {
  constructor(
    public id: string,
    public image: string,
    public name: string,
    public description: string,
    public ingredients: string,
    public steps: Steps[],
  ){}
}

export class Steps {
  constructor(
    public imageUrl: string,
    public text: string,
  ){}
}
