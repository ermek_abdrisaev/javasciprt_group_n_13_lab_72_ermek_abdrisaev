import { Recipies } from './recipies.model';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()

export class RecipiesService {
  recipesChange = new Subject<Recipies[]>();
  recipesFetching = new Subject<boolean>();
  recipesUploading = new Subject<boolean>();
  recipesRemoving = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  private recipies: Recipies[] = [];

  fetchRecipes() {
    this.recipesFetching.next(true);
    this.http.get<{ [id: string]: Recipies }>('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/recipies.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const recipesData = result[id];
          return new Recipies(
            id,
            recipesData.image,
            recipesData.name,
            recipesData.description,
            recipesData.ingredients,
            recipesData.steps,
          );
        });
      }))
      .subscribe(recipies => {
        this.recipies = recipies;
        this.recipesChange.next(this.recipies.slice());
        this.recipesFetching.next(false);
      }, () => {
        this.recipesFetching.next(false);
      });
  }

  fetchRecipe(id: string) {
    return this.http.get<Recipies | null>(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/recipies/${id}.json`)
      .pipe(map(result => {
          if (!result) {
            return null;
          }
          return new Recipies(id, result.image, result.name, result.description, result.ingredients, result.steps);
        })
      );
  }

  addRecipe(recipe: Recipies) {
    const body = {
      image: recipe.image,
      name: recipe.name,
      description: recipe.description,
      ingredients: recipe.ingredients,
      steps: recipe.steps,
    };
    this.recipesUploading.next(true);

    return this.http.post('https://jsclasswork-e9b16-default-rtdb.firebaseio.com/recipies.json', body)
      .pipe(tap(() => {
          this.recipesUploading.next(false);
        }, () => {
          this.recipesUploading.next(false);
        })
      );
  }

  editRecipe(recipe: Recipies) {
    this.recipesUploading.next(true);

    const body = {
      image: recipe.image,
      name: recipe.name,
      description: recipe.description,
      ingredients: recipe.ingredients,
      steps: recipe.steps,
    };
    return this.http.put(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/recipies/${recipe.id}.json`, body)
      .pipe(tap(() => {
          this.recipesUploading.next(false);
        }, () => {
          this.recipesUploading.next(false);
        })
      );
  }

  removeRecipe(id: string) {
    this.recipesRemoving.next(true);
    return this.http.delete(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/recipies/${id}.json`)
      .pipe(tap(() => {
        this.recipesRemoving.next(false);
      }, () => {
        this.recipesRemoving.next(false);
      }));
  }

  removeSteps(id: string) {
    this.recipesRemoving.next(true);
    return this.http.delete(`https://jsclasswork-e9b16-default-rtdb.firebaseio.com/recipies/${id}/steps.json`)
      .pipe(tap(() => {
        this.recipesRemoving.next(false);
      }, () => {
        this.recipesRemoving.next(false);
      }));
  }
}
