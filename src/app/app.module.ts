import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewRecipesComponent } from './new-recipes/new-recipes.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecipiesService } from './shared/recipies.service';
import { HttpClientModule } from '@angular/common/http';
import { AllRecipesComponent } from './all-recipes/all-recipes.component';
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';

@NgModule({
  declarations: [
    AppComponent,
    NewRecipesComponent,
    AllRecipesComponent,
    RecipeDetailsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [RecipiesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
