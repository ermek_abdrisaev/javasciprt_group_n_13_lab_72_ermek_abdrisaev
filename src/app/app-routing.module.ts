import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewRecipesComponent } from './new-recipes/new-recipes.component';
import { AllRecipesComponent } from './all-recipes/all-recipes.component';
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';
import { RecipeResolerService } from './recipe-details/recipe-resoler.service';

const routes: Routes = [
  {path: '', component: AllRecipesComponent},
  {path: 'new-recipes', component: NewRecipesComponent},
  {path: ':id/edit', component: NewRecipesComponent, resolve: {recipe: RecipeResolerService}},
  {path: ':id/recipe-details', component: RecipeDetailsComponent,
    resolve: {
    recipe: RecipeResolerService}
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
