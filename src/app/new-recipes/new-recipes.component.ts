import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { RecipiesService } from '../shared/recipies.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Recipies } from '../shared/recipies.model';

@Component({
  selector: 'app-new-recipes',
  templateUrl: './new-recipes.component.html',
  styleUrls: ['./new-recipes.component.css']
})
export class NewRecipesComponent implements OnInit, OnDestroy {
  recipiesForm!: FormGroup;
  isUploading = false;
  recipiesUploadingSubscription!: Subscription;
  isTouched = true;
  isEdit = false;
  editedId = '';

  constructor(
    private recipiesService: RecipiesService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.recipiesUploadingSubscription = this.recipiesService.recipesUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });

    this.route.data.subscribe(data => {
      const recipe = <Recipies | null>data.recipe;

      if (recipe) {
        this.isEdit = true;
        this.editedId = recipe.id;
        this.setFormValue({
          image: recipe.image,
          name: recipe.name,
          description: recipe.description,
          ingredients: recipe.ingredients,
          steps: [],
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          image: '',
          name: '',
          description: '',
          ingredients: '',
          steps: [],
        });
      }
    });

    this.recipiesForm = new FormGroup({
      image: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      steps: new FormArray([]),
    });
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      this.recipiesForm.setValue(value);
    });
  }

  onSubmit() {
    const id = this.editedId || Math.random().toString();
    const recipe = new Recipies(
      id,
      this.recipiesForm.value.image,
      this.recipiesForm.value.name,
      this.recipiesForm.value.description,
      this.recipiesForm.value.ingredients,
      this.recipiesForm.value.steps,
    );

    const next = () => {
      this.recipiesService.fetchRecipes();
      void this.router.navigate(['/']);
    };

    if (this.isEdit) {
      this.recipiesService.editRecipe(recipe).subscribe(next);
    } else {
      this.recipiesService.addRecipe(recipe).subscribe(next);
    }
  }

  addSteps() {
    this.isTouched = false;
    const steps = <FormArray>this.recipiesForm.get('steps');
    const stepGroup = new FormGroup({
      imageUrl: new FormControl('', Validators.required),
      text: new FormControl('', Validators.required),
    });
    steps.push(stepGroup);
  }

  getStepControls() {
    const steps = <FormArray>(this.recipiesForm.get('steps'));
    return steps.controls;
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.recipiesForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  ngOnDestroy(): void {
    this.recipiesUploadingSubscription.unsubscribe();
  }
}
