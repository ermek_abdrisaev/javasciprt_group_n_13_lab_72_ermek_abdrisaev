import { Component, OnDestroy, OnInit } from '@angular/core';
import { RecipiesService } from '../shared/recipies.service';
import { Recipies } from '../shared/recipies.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-all-recipes',
  templateUrl: './all-recipes.component.html',
  styleUrls: ['./all-recipes.component.css']
})
export class AllRecipesComponent implements OnInit, OnDestroy {
  recipies!: Recipies[];
  recipiesChangeSubscription!: Subscription;
  recipiesFetchingSubscription!: Subscription;
  loading: boolean = false;

  constructor(
    private recipiesService: RecipiesService) { }

  ngOnInit(): void {
    this.recipiesChangeSubscription = this.recipiesService.recipesChange.subscribe((recipies: Recipies[]) =>{
      this.recipies = recipies.reverse();
    });
    this.recipiesFetchingSubscription = this.recipiesService.recipesFetching.subscribe((isFetching: boolean) =>{
      this.loading = isFetching;
    });
    this.recipiesService.fetchRecipes();
  }

  ngOnDestroy(): void {
    this.recipiesChangeSubscription.unsubscribe();
    this.recipiesFetchingSubscription.unsubscribe();
  }

  onDeleteRecipe(id: string){
    this.recipiesService.removeRecipe(id).subscribe(() =>{
      this.recipiesService.fetchRecipes();
    });
  }

}
