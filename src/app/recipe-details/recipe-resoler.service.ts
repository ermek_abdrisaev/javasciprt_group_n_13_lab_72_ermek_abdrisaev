import { Recipies } from '../shared/recipies.model';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { RecipiesService } from '../shared/recipies.service';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class RecipeResolerService implements Resolve<Recipies> {
  constructor(private recipiesService: RecipiesService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Recipies> | Observable<never> {
    const recipeId = <string>route.params['id'];

    return this.recipiesService.fetchRecipe(recipeId).pipe(mergeMap(recipe => {
      if (recipe) {
        return of(recipe);
      }
      void this.router.navigate(['/all-recipes']);
      return EMPTY;
    }));
  }
}
