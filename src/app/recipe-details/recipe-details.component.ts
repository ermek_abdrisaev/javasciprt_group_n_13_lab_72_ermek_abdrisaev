import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { RecipiesService } from '../shared/recipies.service';
import { Recipies } from '../shared/recipies.model';
import { ActivatedRoute, Data, Router } from '@angular/router';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  recipe!: Recipies;
  isUploading = false;
  recipeRemovingSubscription!: Subscription;

  constructor(
    private recipiesService: RecipiesService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data: Data) => {
      this.recipe = <Recipies>data.recipe;
    });

    this.recipeRemovingSubscription = this.recipiesService.recipesFetching.subscribe((isRemoving: boolean) => {
      this.isUploading = isRemoving;
    });
  }

  onDeleteSteps(id: string) {
    this.recipiesService.removeSteps(id).subscribe(() => {
      this.recipiesService.fetchRecipes();
    });
  }
}
